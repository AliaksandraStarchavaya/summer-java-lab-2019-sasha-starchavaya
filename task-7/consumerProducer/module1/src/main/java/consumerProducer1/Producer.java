package consumerProducer1;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class Producer implements Runnable {
    private BlockingQueue<Integer> queue;
    private ExecutorService producers;

    Producer(BlockingQueue<Integer> blockingQueue, ExecutorService executorService) {
        this.producers = executorService;
        this.queue = blockingQueue;
    }

    void start() {
        producers.execute(this::run);
    }

    @Override
    public void run() {
        try {
            int temp = (int) (Math.random() * 200);
            queue.put(temp);
            System.out.println("Produced " + temp);
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
