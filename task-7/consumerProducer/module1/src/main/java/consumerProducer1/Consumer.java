package consumerProducer1;


import java.util.Timer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class Consumer implements Runnable {
    private BlockingQueue<Integer> queue;
    private ExecutorService consumers;

    Consumer(BlockingQueue<Integer> blockingQueue, ExecutorService executorService) {
        this.consumers = executorService;
        this.queue = blockingQueue;
    }

    void start() {
        consumers.execute(this::run);
    }

    @Override
    public void run() {
        try {
            System.out.println("Concumer " + queue.take());
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

