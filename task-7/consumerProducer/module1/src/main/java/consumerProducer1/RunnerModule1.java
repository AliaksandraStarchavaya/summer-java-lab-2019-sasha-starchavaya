package consumerProducer1;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

public class RunnerModule1 {
    public static void main(String[] args) {
        LinkedBlockingDeque<Integer> queue = new LinkedBlockingDeque<>();
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number of producers: ");
        ExecutorService producerService = Executors.newFixedThreadPool(in.nextInt());
        System.out.print("Enter the number of comsumers: ");
        ExecutorService consumerService = Executors.newFixedThreadPool(in.nextInt());
        in.close();
        for (int i = 0; i < 100; i++) {
            new Producer(queue, producerService).start();
            new Consumer(queue, consumerService).start();
        }
        producerService.shutdown();
        consumerService.shutdown();
    }
}
