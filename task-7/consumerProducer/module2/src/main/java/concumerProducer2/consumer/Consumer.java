package concumerProducer2.consumer;

import concumerProducer2.task.Task;
import concumerProducer2.task.TaskA;
import concumerProducer2.task.TaskB;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;

public class Consumer implements Runnable {
    private BlockingQueue<concumerProducer2.task.Task> queue;
    private ExecutorService consumers;

    public Consumer(BlockingQueue<concumerProducer2.task.Task> blockingQueue, ExecutorService executorService) {
        this.consumers = executorService;
        this.queue = blockingQueue;
    }

    public void start() {
        consumers.execute(this::run);
    }

    @Override
    public void run() {
        try {
            Task task = queue.take();
            if (task.getClass().getSimpleName().equals("TaskA")) {
                int returnTaskA = ((TaskA) task).doTaskA();
                System.out.println("Concumer " + task.toString() + "     " + returnTaskA);
            } else if (task.getClass().getSimpleName().equals("TaskB")) {
                System.out.print("Consumer " + task.toString() + "     ");
                ((TaskB) task).doTaskB();
            }
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

