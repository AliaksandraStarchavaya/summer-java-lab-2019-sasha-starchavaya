package concumerProducer2.producer;

import concumerProducer2.task.Task;
import concumerProducer2.task.TaskA;
import concumerProducer2.task.TaskB;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;

public class Producer implements Runnable {
    private BlockingQueue<concumerProducer2.task.Task> queue;
    private ExecutorService producers;

    public Producer(BlockingQueue<concumerProducer2.task.Task> blockingQueue, ExecutorService executorService) {
        this.producers = executorService;
        this.queue = blockingQueue;
    }

    public void start() {
        producers.execute(this::run);
    }

    @Override
    public void run() {
        try {
            Task task = null;
            int a = (int) (Math.random() * 2);
            if (a == 0) {
                task = new TaskA();
            } else if (a == 1) {
                task = new TaskB();
            }
            queue.put(task);
            System.out.println("Produced " + task.toString());
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
