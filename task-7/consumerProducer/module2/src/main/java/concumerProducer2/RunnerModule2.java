package concumerProducer2;

import concumerProducer2.consumer.Consumer;
import concumerProducer2.producer.Producer;
import concumerProducer2.task.Task;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

public class RunnerModule2 {
    public static void main(String[] args) {
        LinkedBlockingDeque<Task> queue = new LinkedBlockingDeque<>();
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number of producers: ");
        ExecutorService producerService = Executors.newFixedThreadPool(in.nextInt());
        System.out.print("Enter the number of comsumers: ");
        ExecutorService consumerService = Executors.newFixedThreadPool(in.nextInt());
        in.close();
        for (int i = 0; i < 100; i++) {
            new Producer(queue, producerService).start();
            new Consumer(queue, consumerService).start();
        }
        producerService.shutdown();
        consumerService.shutdown();
    }
}
