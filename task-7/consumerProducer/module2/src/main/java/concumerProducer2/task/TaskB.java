package concumerProducer2.task;

import java.util.Random;

public class TaskB extends Task {
    private int field = new Random().nextInt(100);

    public void doTaskB() {
        System.out.println(field);
    }

    @Override
    public String toString() {
        return "TaskB{" +
                "field=" + field +
                '}';
    }
}
