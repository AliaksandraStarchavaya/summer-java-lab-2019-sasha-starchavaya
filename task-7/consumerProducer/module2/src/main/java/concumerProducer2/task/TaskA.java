package concumerProducer2.task;

import java.util.Random;

public class TaskA extends Task {
    private int field = new Random().nextInt(100);

    public Integer doTaskA() {
        return field;
    }

    @Override
    public String toString() {
        return "TaskA{" +
                "field=" + field +
                '}';
    }
}
