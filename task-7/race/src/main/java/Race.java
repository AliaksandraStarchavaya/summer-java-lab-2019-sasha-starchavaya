import org.apache.log4j.Logger;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

public class Race {

    private final int trackLength;
    private final int numberOfHorses;
    private final ExecutorService executorService;
    private static final Logger logger = Logger.getLogger(Race.class);

    private Race(int trackLength, int numberOfHorses) {
        this.trackLength = trackLength;
        this.numberOfHorses = numberOfHorses;
        this.executorService = Executors.newFixedThreadPool(numberOfHorses);
    }

    private final Runnable producerTask = new Runnable() {
        public void run() {
            int speed = ThreadLocalRandom.current().nextInt(10, 51);
            int trackLengthHouse = trackLength;
            while (trackLengthHouse > 0) {
                try {
                    Thread.sleep(1000);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                trackLengthHouse -= speed;
                if (trackLengthHouse <= 0) {
                    logger.info("The horse rode at a speed of  " + speed);
                    break;
                }
                logger.info("Horses with a speed of " + speed + " left to finish  " + trackLengthHouse);
            }
        }
    };

    private void start() {
        for (int i = 0; i < numberOfHorses; i++)
            executorService.execute(producerTask);
        executorService.shutdown();
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number of horses: ");
        int numberOfHorses = in.nextInt();
        System.out.print("Enter the length of the track: ");
        int lengthTrack = in.nextInt();
        in.close();
        Race race = new Race(lengthTrack, numberOfHorses);
        race.start();
    }
}
