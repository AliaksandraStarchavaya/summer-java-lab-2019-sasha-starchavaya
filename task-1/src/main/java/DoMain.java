import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DoMain {


    public static void main(String[] args)
    {
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd MMMM ss HH yyyy mm");
        Date date = new Date();

        System.out.println("Hello world: " + formatForDateNow.format(date));

        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.add(Calendar.DAY_OF_MONTH, 14);
        Date newDate = instance.getTime();

        System.out.println("Hello world + 1 week: " + formatForDateNow.format(newDate));

    }
}
