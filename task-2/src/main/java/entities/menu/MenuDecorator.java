package entities.menu;

public class MenuDecorator implements ICook {

    private Menu menu;

    public MenuDecorator(Menu menu) {
        this.menu = menu;
    }

    @Override
    public void add(Menu additive) {
        menu.setName(menu.getName()+"+"+additive.getName());
        menu.setPrice(menu.getPrice() + additive.getPrice());
    }

    @Override
    public int getPrice() {
        return menu.getPrice();
    }

    @Override
    public String toString() {
        return "MenuDecorator{" +
                "menu=" + menu +
                '}';
    }
}
