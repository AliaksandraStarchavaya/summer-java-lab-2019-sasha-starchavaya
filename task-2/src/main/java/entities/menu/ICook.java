package entities.menu;

public interface ICook {
    public void add(Menu menu);
    public int getPrice();
}
