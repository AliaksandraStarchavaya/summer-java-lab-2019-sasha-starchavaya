package entities.client;

public class ClientBuilder {

    private String  name;
    private String surname;
    private String phone;
    private Gender gender;
    private String email;

    public ClientBuilder buildName(String name) {
        this.name = name;
        return this;
    }

    public ClientBuilder buildSurname(String surname) {
        this.surname = surname;
        return this;
    }

    ClientBuilder buildPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public ClientBuilder buildGender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public ClientBuilder buildEmail(String email) {
        this.email = email;
        return this;
    }

    public Client build() {
        Client client = new Client();
        client.setName(name);
        client.setSurname(surname);
        client.setPhone(phone);
        client.setGender(gender);
        client.setEmail(email);
        return client;
    }
}
