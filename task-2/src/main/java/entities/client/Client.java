package entities.client;

public class Client {
    private String name;
    private String surname;
    private String phone;
    private Gender gender;
    private String email;

    void setName(String name) { this.name = name; }

    void setSurname(String surname) {
        this.surname = surname;
    }

    void setPhone(String phone) {
        this.phone = phone;
    }

    void setGender(Gender gender) {
        this.gender = gender;
    }

    void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", phone='" + phone + '\'' +
                ", gender=" + gender +
                ", email='" + email + '\'' +
                '}';
    }
}
