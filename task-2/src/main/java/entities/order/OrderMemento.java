package entities.order;

import java.util.ArrayList;
import java.util.List;

class OrderMemento {
    private List order = new ArrayList();
    private int cost;

    OrderMemento(List order, int cost) {
        this.order = new ArrayList(order);
        this.cost = cost;
    }

    List getOrder() {
        return order;
    }

    int getCost() {
        return cost;
    }
}
