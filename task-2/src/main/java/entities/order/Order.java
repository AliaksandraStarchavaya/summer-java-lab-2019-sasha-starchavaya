package entities.order;

import entities.menu.ICook;
import entities.menu.Menu;

import java.util.ArrayList;
import java.util.List;

public class Order {

    private List order = new ArrayList();
    private int cost;

    public void addMenu(ICook menu) {
        order.add(menu);
        cost = cost + menu.getPrice();
        System.out.println("В Ваш заказ добавлен(а) "+menu.toString());
    }

    public void addMenu(Menu menu) {
        order.add(menu);
        cost = cost + menu.getPrice();
        System.out.println("В Ваш заказ добавлен(а) "+menu.toString());
    }

   public void load(OrderMemento orderMemento){
        order=orderMemento.getOrder();
        cost=orderMemento.getCost();
   }

   public OrderMemento save(){
        return new OrderMemento(order,cost);
   }

    @Override
    public String toString() {
        return "Order{" +
                "order=" + order +
                ", cost=" + cost +
                '}';
    }
}
