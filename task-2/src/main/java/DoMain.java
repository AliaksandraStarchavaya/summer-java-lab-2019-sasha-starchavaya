import entities.client.Client;
import entities.client.ClientBuilder;
import entities.client.Gender;
import entities.menu.ICook;
import entities.menu.Menu;
import entities.menu.MenuDecorator;
import entities.order.Order;
import entities.order.OrderCaretaker;

public class DoMain {


    public static void main(String[] args) {

        //тут я создам элементы меню
        Menu pizzaDelicious = new Menu( "Пицца Вкусная", 14);
        Menu pizzaAromatic = new Menu("Пицца Ароматная", 13);
        Menu pizzaСollectYourself = new Menu("Пицца сотавная", 2);
        Menu ham = new Menu("Ветчина", 4);
        Menu tomato = new Menu("Помидор", 2);
        Menu sausage = new Menu("Колбаска", 4);
        Menu sauceTomato = new Menu("Соус томатный", 2);
        Menu coffe = new Menu("Кофе", 2);
        Menu milk = new Menu("Молоко", 2);

        System.out.println("Создание клиента =>");
        Client client = new ClientBuilder().buildName("Саша")
                .buildSurname("Сторчевая")
                .buildGender(Gender.women)
                .buildEmail("Aleksandra_star@mail.ru")
                .build();
        System.out.println(client);

        System.out.println("Составление заказа =>");
        Order order = new Order();

        //тут я составляю свою пиццу
        ICook pizza = new MenuDecorator(pizzaСollectYourself);
        pizza.add(ham);
        pizza.add(tomato);
        pizza.add(sauceTomato);

        order.addMenu(pizza);
        order.addMenu(coffe);

        System.out.println("Выставление счета и сохранение состояния заказа =>");
        System.out.println(order);
        OrderCaretaker orderCaretaker=new OrderCaretaker();
        orderCaretaker.setMemento(order.save());

        System.out.println("Восстановление заказа, добавление чего-то нового и выставление нового счета =>");
        order.load(orderCaretaker.getMemento());
        order.addMenu(milk);
        System.out.println(order);


    }
}
