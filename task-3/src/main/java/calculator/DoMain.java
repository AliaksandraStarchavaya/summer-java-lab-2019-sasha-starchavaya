package calculator;

import calculator.entity.DecisionExpression;
import calculator.entity.DecisionStep;
import calculator.entity.MathematicalExpression;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class DoMain {

    public static List<MathematicalExpression> expressionСalculation(String path) {
        List<MathematicalExpression> mathematicalExpressionList = new ArrayList();
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String sCurrentLine;
            int idMathematicalExpression = 0;
            while ((sCurrentLine = br.readLine()) != null) {
                String matematicalExpression = sCurrentLine;
                matematicalExpression = matematicalExpression.replace(" ", "")
                        .replace("(-", "(0-")
                        .replace(",-", ",0-");
                if (matematicalExpression.charAt(0) == '-') {
                    matematicalExpression = "0" + matematicalExpression;
                }
                idMathematicalExpression++;
                MathematicalExpression expression = new MathematicalExpression(idMathematicalExpression, matematicalExpression);
                mathematicalExpressionList.add(expression);
            }
        } catch (IOException e) {
            System.out.println("Incorrect path");
            System.exit(0);
        }
        for (MathematicalExpression mathematicalExpression : mathematicalExpressionList) {
            DecisionExpression.solveExpression(mathematicalExpression);

        }
        for (MathematicalExpression mathematicalExpression : mathematicalExpressionList) {
            if (mathematicalExpression.getMathematicalExpression().equals("Incorrect expression")) {
                System.out.printf("%2d. %s%n", mathematicalExpression.getId(), mathematicalExpression.getMathematicalExpression());
            } else
                System.out.printf("%2d. %3d steps; %4.4s result; %s%n", mathematicalExpression.getId(), mathematicalExpression.getNumberSolutionSteps(), String.valueOf(mathematicalExpression.getAnswer()), mathematicalExpression.getMathematicalExpression());
        }
        return mathematicalExpressionList;
    }

    public static void stepOutput(int idExpressional, int idStep, List<MathematicalExpression> mathematicalExpressionList) {
        for (MathematicalExpression mathematicalExpression : mathematicalExpressionList) {
            if (mathematicalExpression.getId() == (idExpressional) && (idStep == 0)) {
                System.out.printf("%2d. %s%n", mathematicalExpression.getId(), mathematicalExpression.getMathematicalExpression());
                break;
            } else if (mathematicalExpression.getId() == (idExpressional) && (mathematicalExpression.getMathematicalExpression().equals("Incorrect expression") || idExpressional < 1 || idExpressional > mathematicalExpressionList.size() || idStep < 0 || idStep > mathematicalExpression.getDecisionStepList().size())) {
                System.out.println("Incorrect value");
                break;
            } else if (mathematicalExpression.getId() == (idExpressional)) {
                for (DecisionStep decisionStep : mathematicalExpression.getDecisionStepList()) {
                    if (decisionStep.getId() == idStep) {
                        System.out.println(decisionStep.getId() + " " + decisionStep.getStep());
                    }
                }
                break;
            }
        }
    }

    public static void main(String[] args) {
        String path = args[0];
        Scanner in = new Scanner(System.in);
        List<MathematicalExpression> mathematicalExpressionList = expressionСalculation(path);
        System.out.print("Enter Expression Number: ");
        int idExpressional = in.nextInt();
        System.out.print("Enter step number: ");
        int idStep = in.nextInt();
        stepOutput(idExpressional, idStep, mathematicalExpressionList);
        in.close();
    }
}


