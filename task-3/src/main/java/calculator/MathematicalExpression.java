package calculator.entity;

import java.util.ArrayList;
import java.util.List;

public class MathematicalExpression {
    private int id;
    private String mathematicalExpression;
    private Double answer;
    private int numberSolutionSteps;
    private List<DecisionStep> decisionStepList = new ArrayList();

    public MathematicalExpression(int id, String mathematicalExpression) {
        this.id = id;
        this.mathematicalExpression = mathematicalExpression;
    }

    public int getId() {
        return id;
    }

    public String getMathematicalExpression() {
        return mathematicalExpression;
    }

    public void setAnswer(Double answer) {
        this.answer = answer;
    }

    public int getNumberSolutionSteps() {
        return numberSolutionSteps;
    }

    public void setNumberSolutionSteps(int numberSolutionSteps) {
        this.numberSolutionSteps = numberSolutionSteps;
    }

    public void addStep(DecisionStep step) {
        decisionStepList.add(step);
    }

    public Double getAnswer() {
        return answer;
    }

    public void setMathematicalExpression(String mathematicalExpression) {
        this.mathematicalExpression = mathematicalExpression;
    }

    public List<DecisionStep> getDecisionStepList() {
        return decisionStepList;
    }

    @Override
    public String toString() {
        return "calculator.entityator.MathematicalExpression{" +
                "mathematicalExpression='" + mathematicalExpression + '\'' +
                ", answer=" + answer +
                ", numberSolutionSteps=" + numberSolutionSteps +
                ", decisionStepList=" + decisionStepList +
                '}';
    }
}
