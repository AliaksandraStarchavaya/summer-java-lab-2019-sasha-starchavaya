package calculator.entity;

import calculator.entity.DecisionStep;
import calculator.entity.MathematicalExpression;

import java.util.ArrayDeque;
import java.util.Stack;
import java.util.StringTokenizer;

public class DecisionExpression {

    private static final String OPERATORS = "+-*/";
    private static final String BRACKETS = "()";
    private static Stack<String> stackOperations = new Stack();
    private static ArrayDeque<String> expressionRPN = new ArrayDeque<>();

    public static int priority(String str) {
        switch (str) {
            case "+":
                return 2;
            case "-":
                return 2;
            case "*":
                return 3;
            case "/":
                return 3;
            case "(":
                return -1;
            case ")":
                return -1;
            default:
                return 0;
        }
    }

    private static boolean isNumber(String token) {
        try {
            Double.parseDouble(token);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    private static boolean isOperator(String token) {
        return OPERATORS.contains(token);
    }

    private static boolean isBrackets(String token) {
        return BRACKETS.contains(token);
    }

    public static void solveExpression(MathematicalExpression mathematicalExpression) {
        reversePolishNotation(mathematicalExpression);
    }

    public static void reversePolishNotation(MathematicalExpression mathematicalExpression) {
        stackOperations.clear();
        expressionRPN.clear();
        StringTokenizer stringTokenizer = new StringTokenizer(mathematicalExpression.getMathematicalExpression(),
                OPERATORS + BRACKETS, true);
        if (!correctBrackets(mathematicalExpression.getMathematicalExpression())) {
            mathematicalExpression.setMathematicalExpression("Incorrect expression");
            return;
        }

        for (int i = 0; i < mathematicalExpression.getMathematicalExpression().length() - 1; i++) {
            Character s = mathematicalExpression.getMathematicalExpression().charAt(i);
            Character s1 = mathematicalExpression.getMathematicalExpression().charAt(i + 1);
            if (isNumber(Character.toString(s)) && s1.equals('(')) {
                mathematicalExpression.setMathematicalExpression("Incorrect expression");
                return;
            }
            if ((isOperator(Character.toString(s1))) && (s.equals('('))) {
                mathematicalExpression.setMathematicalExpression("Incorrect expression");
                return;
            }
            if (isNumber(Character.toString(s1)) && s.equals(')')) {
                mathematicalExpression.setMathematicalExpression("Incorrect expression");
                return;
            }
            if (isOperator(Character.toString(s)) && s1.equals(')')) {
                mathematicalExpression.setMathematicalExpression("Incorrect expression");
                return;
            }
        }

        while (stringTokenizer.hasMoreTokens()) {
            String token = stringTokenizer.nextToken();
            if (!isBrackets(token) && !isOperator(token) && !isNumber(token)) {
                mathematicalExpression.setMathematicalExpression("Incorrect expression");
                return;
            }
            if (isBrackets(token)) {
                if (token.equals("(")) {
                    stackOperations.push(token);
                }
                if (token.equals(")")) {
                    while (!stackOperations.peek().equals("(")) {
                        expressionRPN.add(stackOperations.pop());
                    }
                    stackOperations.pop();
                }
            }
            if (isNumber(token)) {
                expressionRPN.add(token);
            }
            if (isOperator(token)) {
                if (stackOperations.empty() || priority(stackOperations.peek()) < priority(token)) {
                    stackOperations.push(token);
                    continue;
                }
                if (priority(stackOperations.peek()) >= priority(token)) {
                    while (!stackOperations.empty() && priority(stackOperations.peek()) >= priority(token)) {
                        expressionRPN.add(stackOperations.pop());
                    }
                    stackOperations.push(token);
                }
            }
        }
        while (!stackOperations.empty()) {
            expressionRPN.add(stackOperations.pop());
        }
        decisionRPN(mathematicalExpression);
    }

    public static void decisionRPN(MathematicalExpression expressionObj) {
        Stack<Double> decision = new Stack();
        int stepNumber = 0;
        while (!expressionRPN.isEmpty()) {
            String firstToken = expressionRPN.getFirst();
            if (isNumber(firstToken)) {
                decision.push(Double.parseDouble(expressionRPN.pollFirst()));
            } else if (isOperator(firstToken)) {
                Double operator1 = decision.pop();
                Double operator2 = decision.pop();
                Double rezult;
                switch (firstToken) {
                    case "+":
                        rezult = operator2 + operator1;
                        decision.push(rezult);
                        stepNumber++;
                        DecisionStep stepExp = new DecisionStep(stepNumber, operator2 + "+" + operator1);
                        expressionObj.addStep(stepExp);
                        expressionRPN.pop();
                        break;
                    case "-": {
                        rezult = operator2 - operator1;
                        decision.push(rezult);
                        stepNumber++;
                        DecisionStep shag1 = new DecisionStep(stepNumber, operator2 + "-" + operator1);
                        expressionObj.addStep(shag1);
                        expressionRPN.pop();
                        break;
                    }
                    case "/": {
                        if (operator1 == 0) {
                            expressionObj.setMathematicalExpression("Incorrect expression");
                            return;
                        }
                        rezult = operator2 / operator1;
                        decision.push(rezult);
                        stepNumber++;
                        DecisionStep shag1 = new DecisionStep(stepNumber, operator2 + "/" + operator1);
                        expressionObj.addStep(shag1);
                        expressionRPN.pop();
                        break;
                    }
                    case "*": {
                        rezult = operator2 * operator1;
                        decision.push(rezult);
                        stepNumber++;
                        DecisionStep shag1 = new DecisionStep(stepNumber, operator2 + "*" + operator1);
                        expressionObj.addStep(shag1);
                        expressionRPN.pop();
                        break;
                    }
                }
            }
        }
        expressionObj.setNumberSolutionSteps(stepNumber);
        expressionObj.setAnswer(decision.pop());
    }

    public static boolean correctBrackets(String s) {
        String open = "(";
        String close = ")";
        Stack stack = new Stack();
        for (int i = 0; i < s.length(); i++) {
            for (int j = 0; j < open.length(); j++) {
                if (s.charAt(i) == open.charAt(j)) {
                    stack.push(open.charAt(j));
                }
            }
            for (int j = 0; j < close.length(); j++) {
                if (s.charAt(i) == close.charAt(j)) {
                    if (!stack.isEmpty()) {
                        if ((stack.peek() != (Object) '(')
                                && (stack.peek() == (Object) (char)
                                ((int) close.charAt(j) - 2))) {
                            stack.pop();
                        } else if (stack.peek() == (Object) '(' && close.charAt(j) == ')') {
                            stack.pop();
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
            }
        }
        return stack.isEmpty();
    }
}
