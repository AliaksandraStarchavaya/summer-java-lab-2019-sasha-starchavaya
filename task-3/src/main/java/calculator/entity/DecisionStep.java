package calculator.entity;

public class DecisionStep {
    int id;
    String step;

    public DecisionStep(int id, String step) {
        this.id = id;
        this.step = step;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String shag) {
        this.step = shag;
    }

    @Override
    public String toString() {
        return "calculator.DecisionStepecisionStep{" +
                "id=" + id +
                ", step='" + step + '\'' +
                '}';
    }
}
