import library.collection.interfases.Implementation.array.ArrayList;
import library.collection.interfases.Implementation.array.ArrayQueue;
import library.collection.interfases.Implementation.array.ArrayStack;
import library.collection.interfases.Implementation.linked.LinkedList;
import library.collection.interfases.Implementation.linked.LinkedQueue;
import library.collection.interfases.Implementation.linked.LinkedStack;
import library.collection.iterator.Iterator;
import library.map.ArrayMap;
import library.map.Map;
import user.User;


public class Test {
    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(10);
        list.add(20);
        list.add(30);
        System.out.println(list.size());
        list.clear();
        Iterator<Integer> iterator = list.getIterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.getNext());
        }
        list.add(10);
        list.add(80);
        list.add(30);
        list.add(40);
        System.out.println(list.size());
        list.add(null);
        list.add(null);
        list.trim();
        System.out.println(list.size());
        Iterator<Integer> iterator1 = list.getIterator();
        while (iterator1.hasNext()) {
            System.out.println(iterator1.getNext());
        }
        Integer[] mas = {10, 20};
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(40);
        arrayList.add(20);
        arrayList.add(50);
        list.filterMatches(mas);
        list.trim();
        Iterator<Integer> iterator3 = list.getIterator();
        while (iterator3.hasNext()) {
            System.out.println(iterator3.getNext());
        }
        list.filterDifference(arrayList);
        Iterator<Integer> iterator4 = list.getIterator();
        list.trim();
        while (iterator4.hasNext()) {
            System.out.println(iterator4.getNext());
        }
        list.add(150);
        Iterator<Integer> iterator5 = list.getIterator();
        while (iterator5.hasNext()) {
            System.out.println(iterator5.getNext());
        }
        list.remove(1);
        Iterator<Integer> iterator6 = list.getIterator();
        while (iterator6.hasNext()) {
            System.out.println(iterator6.getNext());
        }
        ArrayList<Integer> arrayList1 = new ArrayList();
        arrayList1.add(15);
        arrayList1.add(16);
        arrayList1.add(20);
        arrayList1.add(11);
        list.addAll(arrayList1);
        Iterator<Integer> iterator7 = list.getIterator();
        while (iterator7.hasNext()) {
            System.out.println(iterator7.getNext());
        }
        list.setMaxSize(3);
        Iterator<Integer> iterator8 = list.getIterator();
        while (iterator8.hasNext()) {
            System.out.println(iterator8.getNext());
        }
        ArrayQueue<Integer> queue = new ArrayQueue<>();
        System.out.println(queue.isEmpty());
        queue.push(1);
        queue.push(2);
        queue.push(3);
        System.out.println(queue.isEmpty());
        System.out.println(queue.size());
        Iterator iterator9 = queue.getIterator();
        while (iterator9.hasNext()) {
            System.out.println(iterator9.getNext());
        }
        System.out.println(queue.peek());
        System.out.println(queue.poll());
        System.out.println(queue.size());
        ArrayStack arrayStack = new ArrayStack();
        System.out.println(arrayStack.isEmpty());
        arrayStack.pushAll(queue);
        Iterator iterator10 = queue.getIterator();
        while (iterator10.hasNext()) {
            System.out.println(iterator10.getNext());
        }
        System.out.println(arrayStack.peek());
        System.out.println(arrayStack.pop());
        System.out.println(arrayStack.size());
        arrayStack.push(14);
        arrayStack.push(25);
        arrayStack.push(11);
        System.out.println(arrayStack.size());
        arrayStack.clear();
        System.out.println(arrayStack.size());
        LinkedList<Integer> linkedList = new LinkedList<>();
        linkedList.addAll(list);
        Iterator iterator11 = arrayStack.getIterator();
        while (iterator11.hasNext()) {
            System.out.println(iterator11.getNext());
        }
        LinkedQueue<Integer> linkedQueue = new LinkedQueue<>();
        System.out.println(linkedQueue.isEmpty());
        linkedQueue.push(14);
        linkedQueue.push(12);
        System.out.println(linkedQueue.peek());
        System.out.println(linkedQueue.poll());
        System.out.println(linkedQueue.size());
        LinkedStack<Integer> linkedStack = new LinkedStack<>();
        linkedStack.pushAll(linkedQueue);
        Iterator iterator12 = linkedStack.getIterator();
        while (iterator12.hasNext()) {
            System.out.println(iterator12.getNext());
        }
        linkedStack.push(11);
        linkedStack.push(24);
        linkedStack.clear();
        System.out.println(linkedStack.size());
        Map<Integer, User> map = new ArrayMap();
        System.out.println();
        map.set(0, new User("Sasha1", "milo1@gmail.com", 52, true));
        map.set(1, new User("Sasha2", "milo2@gmail.com", 52, true));
        map.set(2, new User("Sasha3", "milo3n@yandex.ru", 52, false));
        map.set(3, new User("Sasha4", "milo4@gmail.com", 25, true));
        map.set(4, new User("Sasha5", "milo5@gmail.com", 45, false));
        System.out.println(map.getKeys());
        System.out.println(map.getValues());
        map.remove(0);
        map.remove(1);
        map.remove(2);
        System.out.println(map);
        System.out.println("Key" + map.getKeys());
        System.out.println("Values" + map.getValues());
    }
}
