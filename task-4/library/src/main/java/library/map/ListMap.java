package library.map;

import library.collection.interfases.Implementation.linked.LinkedList;
import library.collection.interfases.List;

import java.util.Objects;

public class ListMap<K extends Comparable<K>, V> implements Map<K, V> {
    private LinkedList<Entity<K, V>>[] table;
    private int size;

    public ListMap() {
        this(10);
    }

    public ListMap(int initCapacity) {
        size = 0;
        this.table = new LinkedList[initCapacity];
    }

    @Override
    public List<V> getValues() {
        List<V> result = new LinkedList();
        for (int i = 0; i < table.length; i++) {
            LinkedList<Entity<K, V>> values = table[i];
            if (table[i] != null)
                for (int j = 0; j < values.size(); j++)
                    result.add(values.get(j).getValue());
        }
        return result;
    }

    @Override
    public List<K> getKeys() {
        List<K> result = new LinkedList<K>();
        for (int i = 0; i < table.length; i++) {
            LinkedList<Entity<K, V>> values = table[i];
            if (table[i] != null)
                for (int j = 0; j < values.size(); j++)
                    result.add(values.get(j).getKey());
        }
        return result;
    }

    @Override
    public boolean contains(V value) {
        return getValues().find(value) != -1;
    }

    @Override
    public int clear() {
        int size = 0;
        for (int i = 0; i < table.length; i++) {
            size += table[i].size();
            table[i].clear();
        }
        this.size = 0;
        return size;
    }

    private int hash(int h) {
        h ^= (h >>> 20) ^ (h >>> 12);
        return h ^ (h >>> 7) ^ (h >>> 4);
    }

    @Override
    public void set(K key, V value) {
        set(new Pair<>(key, value));
    }

    private int getIndex(K key) {
        int hash = hash(key.hashCode());
        return indexFor(hash, table.length);
    }

    private boolean isUnique(K key) {
        int index = getIndex(key);
        return table[index] == null;
    }

    public void set(Entity<K, V> entity) {
        if (entity == null) throw new NullPointerException();
        K key = entity.getKey();
        V value = entity.getValue();
        checkPut(key, value);
        if (isOverflow()) increaseSize();
        int index = getIndex(key);
        if (isUnique(key)) table[index] = new LinkedList<>();
        table[index].add(new Pair<>(key, value));
        size++;
    }

    @Override
    public Entity<K, V> remove(K key) {
        if (key == null) throw new NullPointerException();
        Entity<K, V> removedEntity = null;
        int index = getIndex(key);
        LinkedList<Entity<K, V>> list = table[index];
        for (int i = 0; i < list.size(); i++) {
            K entityKey = list.get(i).getKey();
            if (entityKey.equals(key) && entityKey.hashCode() == entityKey.hashCode())
                removedEntity = list.remove(i);
        }
        size--;
        return removedEntity;
    }

    @Override
    public Entity<K, V> remove(Entity<K, V> entity) {
        K key = entity.getKey();
        if (key == null) throw new NullPointerException();
        int index = getIndex(key);
        LinkedList<Entity<K, V>> list = table[index];
        if (list.find(entity) != -1) return remove(entity);
        return null;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    static class Pair<K extends Comparable<K>, V> implements Entity<K, V> {
        K key;
        V value;

        Pair(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public int compareTo(Entity<K, V> o) {
            return this.key.compareTo(o.getKey());
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Pair)) return false;
            Pair<?, ?> pair = (Pair<?, ?>) o;
            return getKey().equals(pair.getKey()) &&
                    getValue().equals(pair.getValue());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getKey(), getValue());
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Pair{");
            sb.append("key=").append(key);
            sb.append(", value=").append(value);
            sb.append('}');
            return sb.toString();
        }
    }

    @Override
    public int size() {
        return size;
    }

    private void checkPut(K key, V val) {
        if (key == null) throw new NullPointerException();
        if (val == null) throw new NullPointerException();
    }

    private boolean isOverflow() {
        return size + 1 > table.length;
    }

    private void increaseSize() {
        int newLength = table.length * 2;
        LinkedList<Entity<K, V>>[] temp = table;
        table = new LinkedList[newLength];
        size = 0;
        putTable(temp);
    }

    private void putTable(LinkedList<Entity<K, V>>[] entitySet) {
        for (int i = 0; i < entitySet.length; i++) {
            LinkedList<Entity<K, V>> entity = entitySet[i];
            if (entity == null) continue;
            for (int j = 0; j < entity.size(); j++) {
                Entity<K, V> element = entity.get(j);
                this.set(element.getKey(), element.getValue());
                size++;
            }
        }
    }

    private int indexFor(int h, int length) {
        return h & (length - 1);
    }
}