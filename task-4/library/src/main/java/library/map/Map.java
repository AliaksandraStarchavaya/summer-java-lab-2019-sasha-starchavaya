package library.map;

import library.collection.interfases.List;

public interface Map<K, V> {
    boolean isEmpty();

    void set(K key, V val);

    void set(Entity<K, V> entity);

    Entity<K, V> remove(K key);

    Entity<K, V> remove(Entity<K, V> entity);

    List<V> getValues();

    List<K> getKeys();

    boolean contains(V value);

    int clear();

    int size();

    interface Entity<K, V> extends Comparable<Entity<K, V>> {
        K getKey();

        V getValue();
    }
}
