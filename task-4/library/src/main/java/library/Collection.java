package library;

import library.collection.iterator.*;

public interface Collection<E> {
    int size();

    Object[] toArray(Object[] array);

    Iterator<E> getIterator();
}

