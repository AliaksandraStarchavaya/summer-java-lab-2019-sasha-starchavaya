package library.collection.iterator;

public interface Iterable<T> {
    Iterator<T> getIterator();
}
