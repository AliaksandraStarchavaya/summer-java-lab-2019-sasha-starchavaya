package library.collection.interfases;

import java.util.Comparator;

import library.Collection;
import library.collection.iterator.*;

public interface Stack<E> extends Collection<E> {
    Iterator getIterator();

    boolean isEmpty();

    E peek();

    E pop();

    void push(E element);

    void pushAll(Collection<? extends E> collection);

    void pushAll(Object[] array);

    int search(E element);

    int clear();

    int size();

    void sort(Comparator<E> comparator);
}
