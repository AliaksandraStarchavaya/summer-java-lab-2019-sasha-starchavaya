package library.collection.interfases.Implementation;

import library.map.Map;

public class Entity<K, V> implements Map.Entity {

    private K key;
    private V value;

    public Entity(K key, V value) {
        if (key != null && value != null) {
            this.key = key;
            this.value = value;
        } else new NullPointerException();
    }

    @Override
    public Object getKey() {
        return key;
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
