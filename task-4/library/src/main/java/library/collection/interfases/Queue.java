package library.collection.interfases;

import library.Collection;
import library.collection.iterator.*;

public interface Queue<E> extends Collection<E> {
    Iterator getIterator();

    boolean isEmpty();

    E peek();

    E poll();

    E pull();

    E remove();

    void push(E element);

    void pushAll(Collection<? extends E> collection);

    void pushAll(Object[] array);

    int search(E element);

    int clear();

    int size();
}
