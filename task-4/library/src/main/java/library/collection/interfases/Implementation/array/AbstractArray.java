package library.collection.interfases.Implementation.array;

import library.AbstractCollection;

public abstract class AbstractArray<E> extends AbstractCollection<E> {
    public boolean isEmpty() {
        return size() == 0;
    }

    protected int find(Object o, Object[] elems) {
        for (int i = 0; i < elems.length; i++)
            if (o != null && o.equals(elems[i])) return i;
        return -1;
    }

    protected E removeFromCollection(int index, int size, Object[] array) {
        E removedElement = (E) array[index];
        int movedElements = size - index - 1;
        if (movedElements > 0)
            System.arraycopy(array, index + 1, array, index, movedElements);
        array[--size] = null;
        return removedElement;
    }
}

