package library.collection.interfases;

import library.Collection;
import library.collection.iterator.Iterator;

public interface List<E> extends Collection<E> {
    Iterator getIterator();

    int add(E element);

    void addAll(Collection<? extends E> collection);

    void addAll(Object[] array);

    E set(int index, E element);

    E remove(int index);

    void clear();

    int find(E element);

    E get(int index);

    Object[] toArray(Object[] array);

    int size();

    void trim();

    void filterMatches(Collection<? extends E> collection);

    void filterMatches(E[] array);

    void filterDifference(Collection<? extends E> collection);

    void filterDifference(E[] array);

    void setMaxSize(int maxSize);

    int getMaxSize();
}
