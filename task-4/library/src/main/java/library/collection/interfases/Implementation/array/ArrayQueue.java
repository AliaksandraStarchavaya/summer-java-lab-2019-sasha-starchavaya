package library.collection.interfases.Implementation.array;

import library.Collection;
import library.collection.interfases.Queue;
import library.collection.iterator.Iterator;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

public class ArrayQueue<E> implements Queue<E> {
    private Object[] arrayQueue;
    private int size;
    private final int DEFAULT_SIZE = 10;

    public ArrayQueue() {
        size = 0;
        arrayQueue = new Object[DEFAULT_SIZE];
    }

    @Override
    public Iterator getIterator() {
        return new ArrayQueueIterator();
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public E peek() {
        if (isEmpty()) throw new NullPointerException();
        return get(size() - 1);
    }

    @Override
    public E poll() {
        return remove(0);
    }

    @Override
    public E pull() {
        return get(size() - 1);
    }

    @Override
    public E remove() {
        return remove(size() - 1);
    }

    @Override
    public void push(E element) {
        if (element == null) throw new NullPointerException();
        boolean isOverflow = size + 1 >= arrayQueue.length;
        if (isOverflow) increaseArraySize();
        arrayQueue[size++] = element;
    }

    @Override
    public void pushAll(Collection<? extends E> collection) {
        Object[] array = new Object[collection.size()];
        array = collection.toArray(array);
        pushAll(array);
    }

    public int search(E element) {
        if (element == null) {
            for (int i = 0; i < size(); i++)
                if (elementValue(i) == null) return i;
        } else {
            for (int i = 0; i < size(); i++)
                if (element.equals(elementValue(i))) return i;
        }
        return -1;
    }

    @Override
    public void pushAll(Object[] array) {
        if (array == null) return;
        Object[] temp = new Object[size + array.length + 1];
        System.arraycopy(arrayQueue, 0, temp, 0, size);
        System.arraycopy(array, 0, temp, size, array.length);
        size += array.length;
        arrayQueue = temp;
    }

    @Override
    public int clear() {
        int oldSize = size();
        for (int i = 0; i < size; i++) arrayQueue[i] = null;
        size = 0;
        return oldSize;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Object[] toArray(Object[] array) {
        System.arraycopy(arrayQueue, 0, array, 0, size);
        return array;
    }

    private E get(int index) {
        return elementValue(index);
    }

    private E elementValue(int index) {
        return (E) arrayQueue[index];
    }

    public E remove(int index) {
        E removedElement = elementValue(index);
        if (removedElement == null) throw new NullPointerException();
        int movedElements = size - index - 1;
        if (movedElements > 0)
            System.arraycopy(arrayQueue, index + 1, arrayQueue, index, movedElements);
        arrayQueue[--size] = null;
        return removedElement;
    }

    private void increaseArraySize() {
        Object[] tempArray = arrayQueue;
        arrayQueue = new Object[arrayQueue.length * 2];
        System.arraycopy(tempArray, 0, arrayQueue, 0, tempArray.length);
    }

    private class ArrayQueueIterator implements Iterator<E> {
        int cursor;
        int lastRet = -1;

        @Override
        public boolean hasNext() {
            return cursor != size;
        }

        @Override
        public E getNext() {
            int i = cursor;
            if (i >= size)
                throw new NoSuchElementException();
            Object[] elementData = ArrayQueue.this.arrayQueue;
            if (i >= elementData.length)
                throw new ConcurrentModificationException();
            cursor = i + 1;
            return elementValue(lastRet = i);
        }

        @Override
        public void remove() {
            if (lastRet < 0)
                throw new IllegalStateException();
            try {
                ArrayQueue.this.remove(lastRet);
                cursor = lastRet;
                lastRet = -1;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public void addBefore(E element) {
            push(element);
        }

        @Override
        public void addAfter(E element) {
            push(element);
        }
    }

    @Override
    public String toString() {
        return "ArrayQueue{" +
                "arrayQueue=" + Arrays.toString(arrayQueue) +
                ", size=" + size +
                ", DEFAULT_SIZE=" + DEFAULT_SIZE +
                '}';
    }
}
