package library.collection.interfases.Implementation.linked;

import library.Collection;
import library.collection.interfases.Queue;
import library.collection.iterator.*;

public class LinkedQueue<E> extends AbstractLinked<E> implements Queue<E> {
    private int size;

    @Override
    public Iterator getIterator() {
        return new LinkedListIterator(0);
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public E peek() {
        if (size == 0) throw new NullPointerException();
        return getLast().item;
    }

    @Override
    public E poll() {
        if (size == 0) throw new NullPointerException();
        Node<E> newHead = getFirst().next;
        E removedElement = getFirst().item;
        setFirst(newHead);
        size--;
        return removedElement;
    }

    @Override
    public E pull() {
        if (size == 0) throw new NullPointerException();
        return getLast().item;
    }

    @Override
    public E remove() {
        if (size == 0) throw new NullPointerException();
        return unlink(getLast());
    }

    @Override
    public void push(E element) {
        linkLast(element);
    }

    @Override
    public void pushAll(Collection<? extends E> collection) {
        Object[] array = new Object[collection.size()];
        array = collection.toArray(array);
        pushAll(array);
    }

    @Override
    public void pushAll(Object[] array) {
        pushArrayPart(array, 0, array.length);
    }

    @Override
    public int clear() {
        int removeNodesNumber = size;
        removeNodes();
        return removeNodesNumber;
    }

    @Override
    public int size() {
        return size;
    }

}
