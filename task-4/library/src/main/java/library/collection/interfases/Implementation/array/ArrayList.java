package library.collection.interfases.Implementation.array;

import library.Collection;
import library.collection.interfases.List;
import library.collection.iterator.Iterator;

import java.util.Arrays;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

public class ArrayList<E extends Comparable<E>> extends AbstractArray<E> implements List<E> {
    private Comparator<E> comparator;
    private Object[] values;
    private int size;
    private Integer maxSize;
    private final int DEFAULT_SIZE = 10;

    public ArrayList() {
        comparator = null;
        values = new Object[DEFAULT_SIZE];
        size = 0;
        maxSize = Integer.MAX_VALUE;
    }

    public ArrayList(Comparator<E> comparator) {
        values = new Object[DEFAULT_SIZE];
        this.comparator = comparator;
    }

    @Override
    public Iterator getIterator() {
        return new ArrayListIterator();
    }

    @Override
    public int add(E element) {
        if (isValidSize(size + 1)) {
            boolean isOverflow = size + 1 >= values.length;
            if (isOverflow) increaseArraySize();
            values[size++] = element;
            if (element != null) {
                sort();
            }
            return find(element);
        }
        return -1;
    }

    @Override
    public void addAll(Collection<? extends E> collection) {
        Object[] array = new Object[collection.size()];
        array = collection.toArray(array);
        addAll(array);
    }

    @Override
    public void addAll(Object[] array) {
        if (array == null) return;
        for (int i = 0; i < array.length; i++) {
            if (this.maxSize > size) {
                boolean isOverflow = size + 1 >= values.length;
                if (isOverflow) increaseArraySize();
                values[size++] = array[i];
                sort();
            } else break;
        }
    }

    @Override
    public E set(int index, E element) {
        values[index] = element;
        sort();
        return element;
    }

    @Override
    public E remove(int index) {
        E removedElement = elementValue(index);
        int movedElements = size - index - 1;
        if (movedElements > 0)
            System.arraycopy(values, index + 1, values, index, movedElements);
        values[--size] = null;
        return removedElement;
    }

    @Override
    public void clear() {
        for (int i = 0; i < size; i++) values[i] = null;
        size = 0;
    }

    @Override
    public int find(E element) {
        if (element == null) {
            for (int i = 0; i < size; i++)
                if (values[i] == null) return i;
        } else {
            for (int i = 0; i < size; i++)
                if (element.equals(values[i])) return i;
        }
        return -1;
    }

    @Override
    public E get(int index) {
        if (index >= size)
            throw new IndexOutOfBoundsException(indexOutOfBoundsMsg(index));
        return elementValue(index);
    }

    private E elementValue(int index) {
        return (E) values[index];
    }


    @Override
    public Object[] toArray(Object[] array) {
        System.arraycopy(values, 0, array, 0, size);
        return array;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void trim() {
        for (int i = 0; i < values.length; i++) {
            if (values[i] == null) {
                try {
                    Object[] temp = values;
                    values = new Object[temp.length - 1];
                    System.arraycopy(temp, 0, values, 0, i);
                    int numberOfItemsAfterTheIndex = temp.length - i - 1;
                    System.arraycopy(temp, i + 1, values, i, numberOfItemsAfterTheIndex);
                    i--;
                } catch (ClassCastException ex) {
                    ex.printStackTrace();
                }
            }
        }
        size = values.length;
    }

    @Override
    public void filterDifference(Collection<? extends E> collection) {
        Object[] array = new Object[collection.size()];
        collection.toArray(array);
        removeExtra(array, false);
        size = values.length;
    }

    @Override
    public void filterDifference(E[] array) {
        removeExtra(array, false);
        size = values.length;
    }

    private void removeExtra(Object[] array, boolean complement) {
        int resultSize = 0;
        for (int i = 0; i < size; i++)
            if ((find(values[i], array) >= 0) == complement)
                values[resultSize++] = values[i];
        if (resultSize != size) {
            for (int i = resultSize; i < size; i++)
                values[i] = null;
            size = resultSize;
        }
    }

    @Override
    public void filterMatches(Collection<? extends E> collection) {
        Object[] array = new Object[collection.size()];
        collection.toArray(array);
        removeExtra(array, true);
        size = values.length;
    }

    @Override
    public void filterMatches(E[] array) {
        removeExtra(array, true);
        size = values.length;
    }

    @Override
    public void setMaxSize(int maxSize) {
        if (maxSize < 0) {
            throw new NegativeArraySizeException("wrong size");
        }
        this.maxSize = maxSize;
        if (size > maxSize) {
            ArrayList<E> arrayList = new ArrayList();
            for (int i = 0; i < maxSize; i++) {
                arrayList.add((E) this.values[i]);
            }
            values = arrayList.values;
            size = maxSize;
        }
    }

    @Override
    public int getMaxSize() {
        if (maxSize == null) {
            return Integer.MAX_VALUE;
        } else
            return maxSize;
    }

    private void sort() {
        for (int i = 0; i < size - 1; i++)
            for (int j = 0; j < size - i - 1; j++)
                if (isBigger(j, j + 1, comparator != null))
                    swap(j, j + 1);
    }

    private boolean isBigger(int biggerIndex, int lessIndex, boolean isComparator) {
        if (isComparator) {
            return elementValue(biggerIndex) == null ||
                    comparator.compare(elementValue(biggerIndex), elementValue(lessIndex)) > 0;
        }
        return elementValue(biggerIndex) == null ||
                elementValue(biggerIndex).compareTo(elementValue(lessIndex)) > 0;
    }

    private void swap(int indexFirst, int indexSecond) {
        E temp = elementValue(indexFirst);
        values[indexFirst] = values[indexSecond];
        values[indexSecond] = temp;
    }


    private boolean isValidSize(int size) {
        return maxSize == null || size <= maxSize;
    }

    private void increaseArraySize() {
        Object[] tempArray = values;
        values = new Object[values.length * 2];
        System.arraycopy(tempArray, 0, values, 0, tempArray.length);
    }

    private String indexOutOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + size;
    }

    private class ArrayListIterator implements Iterator<E> {
        int cursor;
        int lastRet = -1;

        @Override
        public boolean hasNext() {
            return cursor != size;
        }

        @Override
        public E getNext() {
            int i = cursor;
            if (i >= size)
                throw new NoSuchElementException();
            Object[] elementData = ArrayList.this.values;
            if (i >= elementData.length)
                throw new ConcurrentModificationException();
            cursor = i + 1;
            return elementValue(lastRet = i);
        }

        @Override
        public void remove() {
            if (lastRet < 0)
                throw new IllegalStateException();
            try {
                ArrayList.this.remove(lastRet);
                cursor = lastRet;
                lastRet = -1;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        public void addBefore(E element) {
            if (comparator.compare(element, get(0)) <= 0) {
                add(element);
            }
        }

        public void addAfter(E element) {
            if (comparator.compare(element, get(0)) >= 0) {
                add(element);
            }
        }
    }

    @Override
    public String toString() {
        return "ArrayList{" +
                "comparator=" + comparator +
                ", values=" + Arrays.toString(values) +
                ", size=" + size +
                ", maxSize=" + maxSize +
                ", DEFAULT_SIZE=" + DEFAULT_SIZE +
                '}';
    }
}
