package library.collection.interfases.Implementation.linked;

import library.Collection;
import library.collection.interfases.List;
import library.collection.iterator.*;

public class LinkedList<E extends Comparable<E>> extends AbstractLinked<E> implements List<E> {
    private int size;
    private Integer maxSize;

    public LinkedList() {
        size = 0;
        maxSize = Integer.MAX_VALUE;
    }

    @Override
    public Iterator getIterator() {
        return new LinkedListIterator(0);
    }

    @Override
    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public int add(E element) {
        if (isValidSize(1)) {
            linkLast(element);
            sort();
            return size - 1;
        }
        return -1;
    }

    @Override
    public void addAll(Collection<? extends E> collection) {
        Object[] array = new Object[collection.size()];
        array = collection.toArray(array);
        addAll(array);

    }

    @Override
    public void addAll(Object[] array) {
        checkPositionIndex(size);
        int arraySize = array.length;
        if (!isValidSize(arraySize)) {
            int validMaxSize = maxSize - size;
            arraySize = validMaxSize;
        }
        pushArrayPart(array, 0, arraySize);
        sort();
    }

    @Override
    public E set(int index, E element) {
        checkElementIndex(index);
        Node<E> x = node(index);
        E oldVal = x.item;
        x.item = element;
        sort();
        return oldVal;
    }

    @Override
    public E remove(int index) {
        checkElementIndex(index);
        E oldValue = unlink(node(index));
        sort();
        return oldValue;
    }

    @Override
    public void clear() {
        removeNodes();
    }

    @Override
    public int find(E element) {
        return search(element);
    }

    @Override
    public E get(int index) {
        checkElementIndex(index);
        return node(index).item;
    }

    @Override
    public Object[] toArray(Object[] result) {
        int i = 0;
        for (Node<E> x = getFirst(); x != null; x = x.next)
            result[i++] = x.item;
        return result;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void trim() {
        Iterator itr = getIterator();
        while (itr.hasNext())
            if (itr.getNext() == null) itr.remove();
    }

    @Override
    public void filterMatches(Collection<? extends E> collection) {
        Object[] array = new Object[collection.size()];
        collection.toArray(array);
        removeExtra(array, false);
    }

    @Override
    public void filterMatches(E[] array) {
        removeExtra(array, true);
    }

    @Override
    public void filterDifference(Collection<? extends E> collection) {
        Object[] array = new Object[collection.size()];
        collection.toArray(array);
        removeExtra(array, true);

    }

    @Override
    public void filterDifference(E[] array) {
        removeExtra(array, false);
    }

    private void removeExtra(Object[] array, boolean complement) {
        int resultSize = 0;
        for (int i = 0; i < size; i++)
            if ((find(get(i), array) >= 0) == complement)
                set(resultSize++, get(i));
        if (resultSize != size) {
            for (int i = resultSize; i < size; i++)
                remove(i);
            size = resultSize;
        }
    }

    @Override
    public void setMaxSize(int maxSize) {
        if (maxSize < 0) throw new NullPointerException();
        this.maxSize = maxSize;
    }

    @Override
    public int getMaxSize() {
        return maxSize;
    }

    private void checkElementIndex(int index) {
        if (!isElementIndex(index))
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
    }

    private boolean isElementIndex(int index) {
        return index >= 0 && index < size;
    }

    private String outOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + size;
    }

    private void checkPositionIndex(int index) {
        if (!isPositionIndex(index))
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
    }

    private boolean isPositionIndex(int index) {
        return index >= 0 && index <= size;
    }

    private boolean isValidSize(int elementsNumber) {
        return maxSize == null || size + elementsNumber <= maxSize;

    }

    private void sort() {
        int n = size;
        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (get(j + 1) == null || get(j) == null || get(j).compareTo(get(j + 1)) > 0) {
                    E temp = get(j);
                    newValue(j, get(j + 1));
                    newValue(j + 1, temp);
                }
    }

    private void newValue(int index, E element) {
        Node<E> x = node(index);
        x.item = element;
    }
}
