package library.collection.interfases;

public interface Entity<K, V> {
    K getKey();

    V getValue();
}
