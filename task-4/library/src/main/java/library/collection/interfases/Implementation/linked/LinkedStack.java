package library.collection.interfases.Implementation.linked;

import java.util.Comparator;

import library.Collection;
import library.collection.interfases.Stack;
import library.collection.iterator.*;

import java.util.EmptyStackException;

public class LinkedStack<E> extends AbstractLinked<E> implements Stack<E> {
    private int size;

    public LinkedStack() {
        size = 0;
    }

    @Override
    public Iterator getIterator() {
        return new DescendingIterator();
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public E peek() {
        if (isEmpty())
            throw new EmptyStackException();

        return getLast().item;
    }

    @Override
    public E pop() {
        if (size == 0) throw new NullPointerException();
        Node<E> newTail = getLast().prev;
        E removedElement = getLast().item;
        setLast(newTail);
        size--;
        return removedElement;
    }

    @Override
    public void push(E element) {
        if (element == null) throw new NullPointerException();
        if (search(element) == -1) {
            final Node<E> prev = getLast();
            final Node<E> newNode = new Node<E>(prev, element, null);
            setLast(newNode);
            if (prev == null) setFirst(newNode);
            else prev.next = newNode;
            size++;
        }
    }

    @Override
    public void pushAll(Collection<? extends E> collection) {
        Object[] array = new Object[collection.size()];
        array = collection.toArray(array);
        pushAll(array);
    }

    @Override
    public void pushAll(Object[] array) {
        if (array == null) return;
        for (int i = 0; i < array.length; i++) push((E) array[i]);
    }

    @Override
    public int search(E element) {
        int index = 0;
        if (element == null) {
            for (Node<E> x = getFirst(); x != null; x = x.next) {
                if (x.item == null)
                    return index;
                index++;
            }
        } else {
            for (Node<E> x = getFirst(); x != null; x = x.next) {
                if (element.equals(x.item))
                    return index;
                index++;
            }
        }
        return -1;
    }

    @Override
    public int clear() {
        int removeNodesNumber = size;
        removeNodes();
        return removeNodesNumber;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Object[] toArray(Object[] array) {
        int i = 0;
        for (Node<E> x = getFirst(); x != null; x = x.next)
            array[i++] = x.item;
        return array;
    }

    @Override
    public void sort(Comparator<E> comparator) {
        int n = size;
        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (comparator.compare(get(j), get(j + 1)) > 0) {
                    E temp = get(j);
                    newValue(j, get(j + 1));
                    newValue(j + 1, temp);
                }
    }

    private void newValue(int index, E element) {
        Node<E> x = node(index);
        x.item = element;
    }

    private E get(int index) {
        checkElementIndex(index);
        return node(index).item;
    }

    private void checkElementIndex(int index) {
        if (!isElementIndex(index))
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
    }

    private boolean isElementIndex(int index) {
        return index >= 0 && index < size;
    }

    private String outOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + size;
    }

}
