package library.collection.interfases.Implementation.linked;

import java.util.NoSuchElementException;

import library.AbstractCollection;
import library.collection.iterator.*;

public abstract class AbstractLinked<E> extends AbstractCollection<E> {
    private Node<E> first;
    private Node<E> last;

    protected Node<E> getFirst() {
        return first;
    }

    protected void setFirst(Node<E> first) {
        this.first = first;
    }

    protected Node<E> getLast() {
        return last;
    }

    protected void setLast(Node<E> last) {
        this.last = last;
    }

    protected int find(Object o, Object[] elems) {
        for (int i = 0; i < elems.length; i++)
            if (o != null && o.equals(elems[i])) return i;
        return -1;
    }

    @Override
    public Object[] toArray(Object[] result) {
        int i = 0;
        for (Node<E> x = first; x != null; x = x.next)
            result[i++] = x.item;
        return result;
    }

    protected static class Node<E> {
        E item;
        Node<E> next;
        Node<E> prev;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }

    protected Node<E> node(int index) {
        if (index < (size() >> 1)) {
            Node<E> x = first;
            for (int i = 0; i < index; i++)
                x = x.next;
            return x;
        } else {
            Node<E> x = last;
            for (int i = size() - 1; i > index; i--)
                x = x.prev;
            return x;
        }
    }

    public int search(E element) {
        int index = 0;
        if (element == null) {
            for (Node<E> x = first; x != null; x = x.next) {
                if (x.item == null)
                    return index;
                index++;
            }
        } else {
            for (Node<E> x = first; x != null; x = x.next) {
                if (element.equals(x.item))
                    return index;
                index++;
            }
        }
        return -1;
    }

    protected E unlink(Node<E> x) {
        final E element = x.item;
        final Node<E> next = x.next;
        final Node<E> prev = x.prev;
        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
            x.prev = null;
        }
        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
            x.next = null;
        }
        x.item = null;
        setSize(size() - 1);
        return element;
    }

    protected abstract void setSize(int i);

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        Node<E> current = first;
        result.append("[");
        while (current != null) {
            result.append(current.item.toString());
            current = current.next;
            if (current != null) result.append(",");
        }
        result.append("]");
        return result.toString();
    }

    private void linkFirst(E e) {
        final Node<E> f = first;
        final Node<E> newNode = new Node<E>(null, e, f);
        first = newNode;
        if (f == null)
            last = newNode;
        else
            f.prev = newNode;
        setSize(size() + 1);
    }

    public class LinkedListIterator implements Iterator<E> {
        private Node<E> lastReturned;
        private Node<E> next;
        private int nextIndex;

        LinkedListIterator(int index) {
            next = (index == size()) ? null : node(index);
            nextIndex = index;
        }

        @Override
        public boolean hasNext() {
            return nextIndex < size();
        }

        @Override
        public E getNext() {
            lastReturned = next;
            next = next.next;
            nextIndex++;
            return lastReturned.item;
        }

        @Override
        public void remove() {
            if (lastReturned == null)
                throw new IllegalStateException();

            Node<E> lastNext = lastReturned.next;
            unlink(lastReturned);
            if (next == lastReturned)
                next = lastNext;
            else
                nextIndex--;
            lastReturned = null;
        }

        @Override
        public void addBefore(E element) {
            linkFirst(element);
            setSize(size() + 1);
        }

        @Override
        public void addAfter(E element) {
            linkLast(element);
            setSize(size() + 1);
        }

        public boolean hasPrevious() {
            return nextIndex > 0;
        }

        public E previous() {
            if (!hasPrevious())
                throw new NoSuchElementException();
            lastReturned = next = (next == null) ? last : next.prev;
            nextIndex--;
            return lastReturned.item;
        }
    }

    protected void linkLast(E element) {
        final Node<E> prev = last;
        final Node<E> newNode = new Node<E>(prev, element, null);
        last = newNode;
        if (prev == null)
            first = newNode;
        else
            prev.next = newNode;
        setSize(size() + 1);
    }

    public class DescendingIterator implements Iterator<E> {
        private final LinkedListIterator itr = new LinkedListIterator(size());

        public boolean hasNext() {
            return itr.hasPrevious();
        }

        public E getNext() {
            return itr.previous();
        }

        public void remove() {
            itr.remove();
        }

        @Override
        public void addBefore(E element) {
            addBefore(element);
        }

        @Override
        public void addAfter(E element) {
            addAfter(element);
        }
    }

    void removeNodes() {
        for (Node<E> current = first; current != null; ) {
            Node<E> next = current.next;
            current.item = null;
            current.next = null;
            current.prev = null;
            current = next;
        }
        first = last = null;
        setSize(0);
    }

    public void pushArrayPart(Object[] array, int startPosition, int endPosition) {
        if (endPosition == 0) return;
        Node<E> prev;
        prev = last;
        for (int i = 0; i < array.length; i++) {
            Object arrayElement = array[i];
            Node<E> newNode = new Node<E>(prev, (E) arrayElement, null);
            if (prev == null)
                first = newNode;
            else
                prev.next = newNode;
            prev = newNode;
        }
        last = prev;
        setSize(size() + endPosition - startPosition);
    }
}
