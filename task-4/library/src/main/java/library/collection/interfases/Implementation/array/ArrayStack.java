package library.collection.interfases.Implementation.array;

import java.util.Arrays;
import java.util.Comparator;

import library.Collection;
import library.collection.interfases.Stack;
import library.collection.iterator.*;

import java.util.NoSuchElementException;

public class ArrayStack<E> implements Stack<E> {
    private int size;
    private int top;
    private Object[] value;
    private Comparator comparator;

    public ArrayStack() {
        value = new Object[10];
        size = 0;
        top = -1;
    }

    public ArrayStack(Comparator comparator) {
        value = new Object[10];
        size = 0;
        top = -1;
        this.comparator = comparator;
    }

    @Override
    public Iterator getIterator() {
        return new ArrayStackIterator();
    }

    @Override
    public boolean isEmpty() {
        return top == -1;
    }

    @Override
    public E peek() {
        if (isEmpty()) throw new NullPointerException();
        return elementValue(top);
    }

    @Override
    public E pop() {
        if (isEmpty()) throw new NullPointerException();
        E oldValue = elementValue(top);
        value[top--] = null;
        return oldValue;
    }

    @Override
    public void push(E element) {
        if (element == null) throw new NullPointerException();
        boolean isOverflow = size + 1 >= value.length;
        if (search(element) == -1) {
            if (isOverflow) increaseArraySize();
            value[++top] = element;
            size++;
        }
    }

    @Override
    public void pushAll(Collection<? extends E> collection) {
        Object[] array = new Object[collection.size()];
        array = collection.toArray(array);
        pushAll(array);
    }

    @Override
    public void pushAll(Object[] array) {
        if (array == null) return;
        for (int i = 0; i < array.length; i++)
            if (search((E) array[i]) == -1) {
                push((E) array[i]);
            }
        top = size - 1;
    }

    @Override
    public int search(E element) {
        if (element == null) throw new NullPointerException();
        for (int i = 0; i < size; i++)
            if (element.equals(value[i])) return i;
        return -1;
    }

    @Override
    public int clear() {
        int removedNumber = size;
        for (int i = 0; i < size; i++) value[i] = null;
        size = 0;
        top = -1;
        return removedNumber;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Object[] toArray(Object[] array) {
        System.arraycopy(value, 0, array, 0, size);
        return array;
    }

    public void sort(Comparator<E> comparator) {
        for (int i = 0; i < size - 1; i++)
            for (int j = 0; j < size - i - 1; j++)
                if (isBigger(j, j + 1, comparator))
                    swap(j, j + 1);
    }

    private boolean isBigger(int biggerIndex, int lessIndex, Comparator<E> comparator) {
        return elementValue(biggerIndex) == null ||
                comparator.compare(elementValue(biggerIndex), elementValue(lessIndex)) > 0;
    }

    private void swap(int indexFirst, int indexSecond) {
        E temp = elementValue(indexFirst);
        value[indexFirst] = value[indexSecond];
        value[indexSecond] = temp;
    }

    private E elementValue(int index) {
        return (E) value[index];
    }

    private class ArrayStackIterator implements Iterator {
        int cursor = size;
        int lastRet = -1;

        @Override
        public boolean hasNext() {
            return cursor != 0;
        }

        @Override
        public Object getNext() {
            int i = cursor - 1;
            if (i < 0) throw new NoSuchElementException();
            cursor = i;
            return elementValue(lastRet = i);
        }

        @Override
        public void remove() {
            if (lastRet == -1)
                throw new IllegalStateException();
            remove(lastRet);
            cursor = lastRet;
            lastRet = -1;
        }

        private E remove(int index) {
            if (index >= size)
                throw new ArrayIndexOutOfBoundsException(index);
            E oldValue = elementValue(index);

            int numMoved = size - index - 1;
            if (numMoved > 0)
                System.arraycopy(value, index + 1, value, index,
                        numMoved);
            value[--size] = null;
            top--;
            return oldValue;
        }

        @Override
        public void addBefore(Object element) {
            if (comparator.compare(element, peek()) <= 0) {
                push((E) element);
            }
        }

        @Override
        public void addAfter(Object element) {
            if (comparator.compare(element, peek()) >= 0) {
                push((E) element);
            }
        }
    }

    private void increaseArraySize() {
        Object[] tempArray = value;
        value = new Object[value.length * 2];
        System.arraycopy(tempArray, 0, value, 0, tempArray.length);
    }

    @Override
    public String toString() {
        return "ArrayStack{" +
                "size=" + size +
                ", top=" + top +
                ", value=" + Arrays.toString(value) +
                ", comparator=" + comparator +
                '}';
    }
}
