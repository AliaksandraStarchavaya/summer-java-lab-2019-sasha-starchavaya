package library;

import library.collection.iterator.*;

public abstract class AbstractCollection<E> implements Collection<E> {

    public abstract int size();

    public abstract Object[] toArray(Object[] array);

    public abstract Iterator<E> getIterator();

    public boolean isEmpty() {
        return size() == 0;
    }
}
