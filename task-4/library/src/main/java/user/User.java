package user;

import java.util.Objects;

public class User implements Comparable<User> {
    private final String name;
    private final String email;
    private final int age;
    private final boolean male;

    public User(String name, String email, int age, boolean male) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.male = male;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public int getAge() {
        return age;
    }

    public boolean isMale() {
        return male;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getAge() == user.getAge() &&
                isMale() == user.isMale() &&
                Objects.equals(getName(), user.getName()) &&
                Objects.equals(getEmail(), user.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getEmail(), getAge(), isMale());
    }

    @Override
    public int compareTo(User user) {
        return this.age - user.getAge();
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                ", male=" + male +
                '}';
    }
}